CREATE DATABASE book_store DEFAULT CHARSET=utf8mb4 ;


CREATE TABLE `book` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `book_name` varchar(64) NOT NULL COMMENT '图书名称',
  `book_desc` varchar(128) NOT NULL COMMENT '图书描述',
  `author_name` varchar(64) NOT NULL COMMENT '作者名称',
  `author_profile` varchar(128) NOT NULL COMMENT '作者简介',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='图书主表';


CREATE TABLE `sys_user` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `user_profile` varchar(128) NOT NULL COMMENT '用户头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';