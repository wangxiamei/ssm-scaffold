package com.wangxiamei.model;

import lombok.Data;

/**
 * @author wangxiamei
 */
@Data
public class SysUserLoginVo {
    private String username;
    private String password;
}
