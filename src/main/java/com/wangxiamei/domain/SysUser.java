package com.wangxiamei.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangxiamei
 */
@Data
@TableName("sys_user")
public class SysUser {
    private Long id;

    @NotBlank
    private String username;
    @NotBlank
    @JsonIgnore
    private String password;

    private String userProfile;
}
