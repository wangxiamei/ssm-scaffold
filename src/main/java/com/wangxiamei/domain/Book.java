package com.wangxiamei.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author wangxiamei
 */
@TableName("book")
@Data
public class Book {

    private Long id;

    @NotBlank
    private String bookName;
    @NotBlank
    private String bookDesc;
}
