package com.wangxiamei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wangxiamei.domain.Book;
import com.wangxiamei.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wangxiamei.mapper.BookMapper;
import org.springframework.util.StringUtils;

/**
 * @author wangxiamei
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookMapper bookMapper;

    @Override
    public Book getById(Long id) {
        return bookMapper.selectById(id);
    }

    @Override
    public Page<Book> list(Book book, Page<Book> page) {
        LambdaQueryWrapper<Book> lambdaQuery = Wrappers.lambdaQuery();
        if (StringUtils.hasLength(book.getBookName())) {
            lambdaQuery.like(Book::getBookName, book.getBookName());
        }
        return bookMapper.selectPage(page, lambdaQuery);

    }

    @Override
    public boolean addBook(Book book) {
        if (book.getId() != null) {
            return bookMapper.updateById(book) > 0;
        } else {
            return bookMapper.insert(book) > 0;
        }
    }

    @Override
    public boolean deleteById(Long id) {
        return bookMapper.deleteById(id) > 0;


    }
}
