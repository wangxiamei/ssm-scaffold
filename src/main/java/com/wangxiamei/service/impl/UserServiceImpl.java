package com.wangxiamei.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wangxiamei.sys.PasswordEncode;
import com.wangxiamei.domain.SysUser;
import com.wangxiamei.model.SysUserLoginVo;
import com.wangxiamei.mapper.UserMapper;
import com.wangxiamei.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wangxiamei
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;


    @Override
    public SysUser login(SysUserLoginVo loginVo) {
        LambdaQueryWrapper<SysUser> lambdaQuery = Wrappers.lambdaQuery();
        lambdaQuery.eq(SysUser::getUsername, loginVo.getUsername());
        lambdaQuery.eq(SysUser::getPassword, PasswordEncode.encode(loginVo.getPassword()));
        return userMapper.selectOne(lambdaQuery);

    }
    @Override
    public boolean register(SysUser sysUser) {
       sysUser.setPassword(PasswordEncode.encode(sysUser.getPassword()));
       return  userMapper.insert(sysUser) > 0;
    }
}
