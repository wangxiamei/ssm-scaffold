package com.wangxiamei.service;

import com.wangxiamei.domain.SysUser;
import com.wangxiamei.model.SysUserLoginVo;

/**
 * @author wangxiamei
 */
public interface UserService {
    SysUser login(SysUserLoginVo loginVo);

    boolean register(SysUser sysUser);
}
