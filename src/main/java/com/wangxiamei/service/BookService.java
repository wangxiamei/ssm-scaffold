package com.wangxiamei.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wangxiamei.domain.Book;

/**
 * @author wangxiamei
 */
public interface BookService {

    Book getById(Long id);

    Page<Book> list(Book book, Page<Book> page);

    boolean addBook(Book book);

    boolean deleteById(Long id);
}
