package com.wangxiamei.sys;


import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;


/**
 * @author wangxiamei
 */
@Slf4j
public class PasswordEncode {

    private static final byte[] key = "wangxiamei123456".getBytes();
    private static final String transformation = "AES/ECB/PKCS5Padding";
    private static final String algorithm = "AES";

    public static String encode(String data) {
        if(!StringUtils.hasLength(data)){
            return null;
        }
        try {
            Cipher cipher = Cipher.getInstance(transformation);
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, algorithm));
            return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
        } catch (Exception e) {
            log.error("encode error,", e);
            return null;
        }

    }

    public static String decode(String data) {
        if(!StringUtils.hasLength(data)){
            return null;
        }
        try {
            byte[] decode = Base64.getDecoder().decode(data);
            Cipher cipher = Cipher.getInstance(transformation);
            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, algorithm));
            return new String(cipher.doFinal(decode));
        } catch (Exception e) {
            log.error("decode error,", e);
            return null;
        }

    }

//    public static void main(String[] args)  {
//        try {
//            String data = "Hello World"; // 待加密的明文
//            String key = "12345678abcdefgh"; // key 长度只能是 16、25 或 32 字节
//
//            String encode =  encode(data);
//
//            System.out.println(encode);
//            String decode = decode(encode);
//            System.out.println(decode);
//        } catch (Exception e){
//            e.printStackTrace();
//        }

//    }

}
