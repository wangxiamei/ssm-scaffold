package com.wangxiamei.sys;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author wangxiamei
 */
@Controller
public class PingController {
    @GetMapping("ping")
    public String ping() {
        return "pong";
    }
}
