package com.wangxiamei.sys.aop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author wangxiamei
 */
@ControllerAdvice
@Slf4j
public class MyControllerAdvice {

    @ExceptionHandler
    public ModelAndView handleException(@Autowired Exception e){
        log.error("error",e);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("msg",e.getMessage());
        modelAndView.setViewName("error");
        return modelAndView;

    }






}
