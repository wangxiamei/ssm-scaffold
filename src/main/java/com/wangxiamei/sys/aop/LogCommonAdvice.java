package com.wangxiamei.sys.aop;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author wangxiamei
 */
@Aspect
@Slf4j
@Component
public class LogCommonAdvice {

    @Around("execution(* com.wangxiamei.service.impl.*.*(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        ObjectMapper objectMapper = new ObjectMapper();
        log.info("before: method:{}", joinPoint.getSignature());
        Object[] pointArgs = joinPoint.getArgs();
        if (pointArgs != null && pointArgs.length > 0) {
            for (int i = 0; i < pointArgs.length; i++) {
                log.info("before: arg-{}:{}", i, objectMapper.writeValueAsString(pointArgs[i]));
            }
        }
        Object proceed = joinPoint.proceed();
        log.info("after: method:{},result:{}", joinPoint.getSignature(), objectMapper.writeValueAsString(proceed));
        return proceed;


    }

}
