package com.wangxiamei.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wangxiamei.service.BookService;
import com.wangxiamei.domain.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author wangxiamei
 */
@Controller
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/{id}")
    public Book getById(@PathVariable Long id) {
        return bookService.getById(id);
    }



    @GetMapping("/delete/{id}")
    public String deleteById(@PathVariable Long id) {
        bookService.deleteById(id);
        return "main";
    }


    @GetMapping("/editBookHtml/{id}")
    public String editBookhtml(@PathVariable Long id,Model model) {
        Book book =  bookService.getById(id);
        model.addAttribute("book", book);
        return "editBook";
    }

    @GetMapping
    public String list(Book book, Page<Book> page, Model model) {
        Page<Book> result = bookService.list(book, page);
        model.addAttribute("books", result);
        return "book";
    }

    @PostMapping("/addBook")
    public String addBook(@Validated Book book, Model model) {
        boolean result = bookService.addBook(book);
        if (result) {
            model.addAttribute("activeName", "/books?current=1&size=10");
            return "main";
        } else {
            model.addAttribute("msg", "添加失败，请重试");
            model.addAttribute("book", book);

            return "addBook";
        }
    }


}
