package com.wangxiamei.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wangxiamei.domain.SysUser;
import com.wangxiamei.model.SysUserLoginVo;
import com.wangxiamei.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Base64Utils;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * @author wangxiamei
 */
@Controller
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public String login(@Validated SysUserLoginVo loginVo, HttpServletResponse response, Model model) throws JsonProcessingException {
        SysUser loginUser = userService.login(loginVo);
        if (loginUser != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            String valueAsString = objectMapper.writeValueAsString(loginUser);
            Cookie cookie = new Cookie("loginUser", Base64Utils.encodeToString(valueAsString.getBytes()));
            response.addCookie(cookie);
            model.addAttribute("loginUser", valueAsString);
            model.addAttribute("activeName", "/books?current=1&size=10");

            return "redirect:/html/main.html";

        } else {
            model.addAttribute("msg", "用户名或密码错误，请重试");

            return "redirect:/html/login.html";
        }
    }

    @GetMapping("/logout")
    public String logout(HttpServletResponse response) {
        Cookie cookie = new Cookie("loginUser", null);
        response.addCookie(cookie);
        return "redirect:/html/login.html";

    }

    @PostMapping("/register")
    public String register(@Validated SysUser sysUser, Model model, MultipartFile multipartFile) throws Exception {

        String originalFilename = multipartFile.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        if (".jpg".equals(suffix) || ".png".equals(suffix)) {
            String newFileName = sysUser.getUsername() + suffix;

            // 根路径，在 resources/static/upload
            String basePath = ResourceUtils.getURL("classpath:").getPath() + "userImage/";
            // 建立新的文件
            File fileExist = new File(basePath);
            // 文件夹不存在，则新建
            if (!fileExist.exists()) {

                fileExist.mkdirs();
            }
            // 获取文件对象
            File file = new File(basePath, newFileName);
            // 完成文件的上传
            multipartFile.transferTo(file);

            sysUser.setUserProfile(newFileName);
            boolean result = userService.register(sysUser);
            if (result) {
                return "redirect:/html/login.html";
            } else {
                model.addAttribute("msg", "注册失败，请重试");
            }
        } else {
            model.addAttribute("msg", "图片格式仅支持.png,.jpg");
        }
        model.addAttribute("sysUser", sysUser);
        return "register";

    }


}
