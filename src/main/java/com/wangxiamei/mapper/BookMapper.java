package com.wangxiamei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangxiamei.domain.Book;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangxiamei
 */
@Mapper
public interface BookMapper extends BaseMapper<Book> {
}
