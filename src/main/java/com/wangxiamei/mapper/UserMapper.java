package com.wangxiamei.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wangxiamei.domain.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author wangxiamei
 */
@Mapper
public interface UserMapper extends BaseMapper<SysUser> {
}
