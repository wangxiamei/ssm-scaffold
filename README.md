## ssm脚手架
基于Spring Boot、Mybatis-Plus、Bootstrap搭建的一个后台管理系统，前后端代码一体的单体应用。
基于此脚手架模板，会开发一个maven和idea的插件，根据数据库表结构一键生成整个框架以及CRUD代码。
### 环境
- Open JDK 1.8
- Spring Boot 2.5.12
- Mybatis-plus 3.4.13
- Maven 3.6.3
- Bootstrap 
### 功能
- 使用Interceptor实现登录验证
- controller层统一封装异常处理
- service统一记录参数和返回值日志
- 集成Thymeleaf模板引擎
- 使用Bootstrap实现前端UI
### 项目结构
.                                                                                 
├── README.md                                                                     
├── pom.xml                                                                       
├── src                                                                           
│   ├── main                                                                      
│   │   ├── java                                                                  
│   │   │   └── com                                                               
│   │   │       └── wangxiamei                                                    
│   │   │           ├── BookStoreApplication.java                                 
│   │   │           ├── controller                                                
│   │   │           │   ├── BookController.java                                   
│   │   │           │   └── UserController.java                                   
│   │   │           ├── domain                                                    
│   │   │           │   ├── Book.java                                             
│   │   │           │   └── SysUser.java                                          
│   │   │           ├── mapper                                                    
│   │   │           │   ├── BookMapper.java                                       
│   │   │           │   └── UserMapper.java                                       
│   │   │           ├── model                                                     
│   │   │           │   └── SysUserLoginVo.java                                   
│   │   │           ├── service                                                   
│   │   │           │   ├── BookService.java                                      
│   │   │           │   ├── UserService.java                                      
│   │   │           │   └── impl                                                  
│   │   │           │       ├── BookServiceImpl.java                              
│   │   │           │       └── UserServiceImpl.java                              
│   │   │           └── sys                                                       
│   │   │               ├── Constants.java                                        
│   │   │               ├── LoginInterceptor.java                                 
│   │   │               ├── PasswordEncode.java                                   
│   │   │               ├── PingController.java                                   
│   │   │               ├── aop                                                   
│   │   │               │   ├── LogCommonAdvice.java                              
│   │   │               │   └── MyControllerAdvice.java                           
│   │   │               └── config                                                
│   │   │                   └── MybatisPlusConfig.java                            
│   │   └── resources                                                             
│   │       ├── application.yml                                                   
│   │       ├── book_store.sql                                                    
│   │       ├── favicon.ico                                                       
│   │       ├── logback.xml                                                       
│   │       ├── static                                                            
│   │       │   ├── favicon.ico                                                   
│   │       │   ├── html                                                          
│   │       │   │   ├── 404.html                                                  
│   │       │   │   ├── addBook.html                                              
│   │       │   │   ├── book.html                                                 
│   │       │   │   ├── editBook.html                                             
│   │       │   │   ├── error.html                                                
│   │       │   │   ├── favicon.ico                                               
│   │       │   │   ├── login.html                                                
│   │       │   │   ├── main.html                                                 
│   │       │   │   └── register.html                                             
│   │       │   └── image                                                         
│   │       │       ├── liuyuning.png                                             
│   │       │       └── maotouying.png                                            
│   │       └── userImage                                                         
│   └── test                                                                      
│       ├── java                                                                  
│       │   └── com                                                               
│       │       └── wangxiamei                                                    
│       │           └── BookTest.java                                             
│       └── resources                                                             
│           └── application.yml                                                   
└── ~                                                                             
    └── logs                                                                      
        ├── sys-error.log                                                         
        └── sys-info.log             
